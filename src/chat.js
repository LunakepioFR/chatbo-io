/* eslint-disable default-case */
/* eslint-disable no-unused-vars */
/* eslint-disable no-console */

import samAvatar from './assets/img/sam.png';
import googleAvatar from './assets/img/okgoogle.png';
import siriAvatar from './assets/img/siri.png';

const Chat = class Chat {
  constructor() {
    this.el = document.querySelector('#whatsapp');

    this.bots = [];

    this.run();
  }

  takeBots(type) {
    this.bots = this.bots.concat(type);
  }

  searchBot(botId) {
    return this.bots.find((bot) => bot.id === botId);
  }

  removeRun() {
    document.addEventListener('animationend', () => {
      const elements = document.querySelectorAll('.chat');
      const elements2 = document.querySelectorAll('.whole-container');
      elements.forEach((element) => {
        element.classList.remove('user-run');
      });
      elements2.forEach((element) => {
        element.classList.remove('run');
      });
    });
  }

  findCmd(cmd, strArray) {
    for (let i = 1; i <= this.bots.length; i += 1) {
      const botTried = this.searchBot(i);
      for (let j = 0; j < botTried.commands.length; j += 1) {
        const command = botTried.commands[j];
        for (let y = 0; y < command.length; y += 1) {
          if (cmd === command[y]) {
            if (cmd === 'weather' || cmd === 'Weather') {
              let cmdIndex = strArray.indexOf('in');
              cmdIndex += 1;
              this.getWeather(botTried, strArray[cmdIndex]);
              return true;
            }
            this.execute(botTried, j);
          }
        }
      }
    }
    return false;
  }

  execute(botTried, j) {
    const botChosen = botTried;
    switch (botChosen.id) {
      // Siri
      case 1:
        switch (j) {
          case 0:
            this.hello(botChosen);
            break;
          case 1:
            this.help(botChosen);
            break;
          case 2:
            this.giveJoke(botChosen);
            break;
          case 3:
            this.apple(botChosen);
            break;
        }
        break;
      // okGoogle
      case 2:
        switch (j) {
          case 0:
            this.hello(botChosen);
            break;
          case 1:
            this.giveDate(botChosen);
            break;
          case 2:
            this.getBitcoinValue(botChosen);
            break;
          case 3:
            this.android(botChosen);
            break;
        }
        break;
      // Sam
      case 3:
        switch (j) {
          case 0:
            this.hello(botChosen);
            break;
          case 1:
            this.getweather(botChosen);
            break;
          case 2:
            this.getNorrisFact(botChosen);
            break;
        }
        break;
    }
  }

  giveDate(botChosen) {
    const { id } = botChosen;
    const today = new Date();
    const day = today.getDate();
    const month = today.getMonth();
    const hour = today.getHours();
    const minutes = today.getMinutes();
    const message = `Nous sommes le ${day}/${month}, il est actuellement ${hour}:${minutes}`;

    this.renderBotMessage(id, message);
  }

  apple(botChosen) {
    const { id } = botChosen;
    const messageSiri = 'Obviously Apple is better';
    const messageGoogle = 'no Android is';
    this.renderBotMessage(id, messageSiri);
    setTimeout(() => {
      this.renderBotMessage(2, messageGoogle);
    }, 1500);
  }

  android(botChosen) {
    const { id } = botChosen;
    const messageGoogle = 'Obviously Android is better';
    const messageSiri = 'no Apple is';
    this.renderBotMessage(id, messageGoogle);
    setTimeout(() => {
      this.renderBotMessage(1, messageSiri);
    }, 1500);
  }

  hello(botChosen) {
    const { id } = botChosen;
    const message = 'Hello, ravis de te rencontrer';

    this.renderBotMessage(id, message);
  }

  help(botChosen) {
    const { id } = botChosen;
    const message = "here's what the bots can do,</br></br><strong>Siri</strong><br><ul><li>'Help', gives you the list of commands</li><li>'hello', have all the bots welcome you</li><li>'joke', tells you a random joke (could be surprising 🤭)</li><li>'apple', tells if Apple is better than android</li></ul></br><strong>okGoogle</strong><br><ul><li>'hello', have all the bots welcome you</li><li>'bitcoin' | 'btc', gives you the current value of bitcoin</li><li>'android', tells you if android is better</li></ul></br><strong>Sam</strong><br><ul><li>'hello', have all the bots welcome you</li><li>'wheater' in 'args', must be used this way : 'weather in nameOfCity', gives you the current weather in the said city</li><li>'chuck', gives you a random Chuck Norris fact</li></ul>";

    this.renderBotMessage(id, message);
  }

  giveJoke(botChosen) {
    const { id } = botChosen;
    const url = 'https://v2.jokeapi.dev/joke/Any?type=twopart';

    fetch(url).then((res) => res.json()).then((res) => {
      const jokeSetup = `${res.setup}`;
      const jokeDelivery = `${res.delivery}`;

      this.renderBotMessage(id, jokeSetup);
      setTimeout(() => {
        this.renderBotMessage(id, jokeDelivery);
      }, 3000);
    });
  }

  renderBotMessage(id, message) {
    const convo = document.querySelector('.conversation');
    const el1 = document.getElementById('bot-1');
    const el2 = document.getElementById('bot-2');
    const el3 = document.getElementById('bot-3');
    const currentDate = new Date();
    const hh = currentDate.getHours();
    const minutes = currentDate.getMinutes();
    let mm = minutes;
    if (minutes < 10) {
      mm = `0${minutes}`;
    }
    switch (id) {
      case 1:
        el1.classList.add('active');
        convo.innerHTML += `<div class="whole-container run"><div class="message-container"><div class="bot-avatar"><img src="${siriAvatar}"></img></div><div class="chat received">${message}</div></div><div class="message-time">${hh}:${mm}</div></div>`;
        convo.scrollTop = convo.scrollHeight - convo.clientHeight;
        break;
      case 2:
        el2.classList.add('active');
        convo.innerHTML += `<div class="whole-container run"><div class="message-container"><div class="bot-avatar"><img src="${googleAvatar}"></img></div><div class="chat received">${message}</div></div><div class="message-time">${hh}:${mm}</div></div>`;
        convo.scrollTop = convo.scrollHeight - convo.clientHeight;
        break;
      case 3:
        el3.classList.add('active');
        convo.innerHTML += `<div class="whole-container run"><div class="message-container"><div class="bot-avatar"><img src="${samAvatar}"></img></div><div class="chat received">${message}</div></div><div class="message-time">${hh}:${mm}</div></div>`;
        convo.scrollTop = convo.scrollHeight - convo.clientHeight;
        break;
    }
    const key = 'chat';
    localStorage.setItem(key, convo.innerHTML);
  }

  renderMessageSend(message) {
    const { text } = message;
    const convo = document.querySelector('.conversation');
    const currentDate = new Date();
    const hh = currentDate.getHours();
    const minutes = currentDate.getMinutes();
    let mm = minutes;
    if (minutes < 10) {
      mm = `0${minutes}`;
    }
    convo.innerHTML += `<div class="chat sent user-run">${text}</div><div class="message-sent-time">${hh}:${mm}</div>`;
    convo.scrollTop = convo.scrollHeight - convo.clientHeight;
    const key = 'chat';
    localStorage.setItem(key, convo.innerHTML);
  }

  getBitcoinValue(botChosen) {
    const { id } = botChosen;
    const url = 'https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD,JPY,EUR';

    fetch(url).then((res) => res.json()).then((res) => {
      const message = `a bitcoin is worth : ${res.EUR}€`;

      this.renderBotMessage(id, message);
    });
  }

  getWeather(botChosen, city) {
    const { id } = botChosen;
    const apiKey = '8c19102bf0c2d10276ee905ca3c6791c';
    const cityName = city;
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${cityName}&units=metric&appid=${apiKey}`;

    fetch(url).then((res) => res.json()).then((res) => {
      const message = `Here are today's ongoing weather in ${res.name}, it seems there's a ${res.weather[0].description} and the temperature is ${res.main.temp}°C`;

      this.renderBotMessage(id, message);
    });
  }

  showHistoric() {
    document.addEventListener('DOMContentLoaded', () => {
      const convo = document.getElementById('convo');
      const chat = localStorage.getItem('chat');
      if (chat) {
        convo.innerHTML += `${chat}`;
      }
    });
  }

  getNorrisFact(botChosen) {
    const { id } = botChosen;
    const url = 'https://api.chucknorris.io/jokes/random';
    fetch(url).then((res) => res.json()).then((res) => {
      const message1 = "here's a Chuck Norris Fact";
      const message2 = `${res.value}`;

      this.renderBotMessage(id, message1);
      setTimeout(() => {
        this.renderBotMessage(id, message2);
      }, 1500);
    });
  }

  showBot() {
    document.addEventListener('DOMContentLoaded', () => {
      const el = document.querySelector('.contact');
      for (let i = 1; i <= this.bots.length; i += 1) {
        const botTried = this.searchBot(i);
        switch (i) {
          case 1:
            el.innerHTML += `<div id="bot-1" class="empty-contact"><div class="avatar"><img src="${siriAvatar}"></img></div><div class="name">${botTried.name}</div></div>`;
            break;
          case 2:
            el.innerHTML += `<div id="bot-2" class="empty-contact"><div class="avatar"><img src="${googleAvatar}"></img></div><div class="name">${botTried.name}</div></div>`;
            break;
          case 3:
            el.innerHTML += `<div id="bot-3" class="empty-contact"><div class="avatar"><img src="${samAvatar}"></img></div><div class="name">${botTried.name}</div></div>`;
            break;
        }
      }
    });
  }

  typingMessage() {
    document.addEventListener('DOMContentLoaded', () => {
      const el = document.querySelector('.send-message input');
      const sendButton = document.querySelector('.send-message button');
      const send = () => {
        const input = document.getElementById('userInput');
        const text = input.value;
        const message = {
          text
        };
        if (input.value !== '') {
          this.removeRun();
          this.renderMessageSend(message);
          const words = text.split(' ');
          const el1 = document.getElementById('bot-1');
          const el2 = document.getElementById('bot-2');
          const el3 = document.getElementById('bot-3');

          el1.classList.remove('active');
          el2.classList.remove('active');
          el3.classList.remove('active');
          let testcmd;
          setTimeout(() => {
            for (let i = 0; i < words.length; i += 1) {
              testcmd = this.findCmd(words[i], words);
            }
          }, 1000);
          input.value = '';
        }
      };
      el.addEventListener('keypress', (e) => {
        if (e.keyCode === 13) {
          const input = document.getElementById('userInput');
          const text = input.value;
          const message = {
            text
          };
          if (input.value !== '') {
            this.removeRun();
            this.renderMessageSend(message);
            const words = text.split(' ');
            const el1 = document.getElementById('bot-1');
            const el2 = document.getElementById('bot-2');
            const el3 = document.getElementById('bot-3');

            el1.classList.remove('active');
            el2.classList.remove('active');
            el3.classList.remove('active');
            let testCmd;
            setTimeout(() => {
              for (let i = 0; i < words.length; i += 1) {
                testCmd = this.findCmd(words[i], words);
              }
            }, 1000);
            input.value = '';
          }
        }
      });
      sendButton.addEventListener('click', send);
    });
  }

  cleanLocalStorage() {
    const button = document.getElementById('clean');
    const convo = document.getElementById('convo');
    button.addEventListener('click', () => {
      localStorage.clear();
      convo.innerHTML = '';
    });
  }

  run() {
    this.typingMessage();
    this.showBot();
    this.showHistoric();
    this.cleanLocalStorage();
  }
};

export default Chat;
