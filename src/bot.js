const Bot = class Bot {
  constructor(id, name, avatar) {
    this.id = id;
    this.name = name;
    this.avatar = avatar;
    this.commands = [];
  }

  takeCmd(cmd) {
    this.commands.push(cmd);
  }
};

export default Bot;
