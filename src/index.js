import Bot from './bot';
import Chat from './chat';
import './index.scss';
import send from './assets/img/send.png';

const el = document.querySelector('.send-message');
el.innerHTML += `<button id="send-button" type="button"><img src="${send}"></img></button>`;

const chat = new Chat();
const siri = new Bot(1, 'Siri', 'siriAvatar');
const okGoogle = new Bot(2, 'okGoogle', 'googleAvatar');
const sam = new Bot(3, 'Sam', 'samAvatar');
siri.takeCmd(['hello', 'Hello']);
okGoogle.takeCmd(['hello', 'Hello']);
sam.takeCmd(['hello', 'Hello']);

siri.takeCmd(['help', 'cmd', 'how', 'Help', 'Cmd', 'How']);
siri.takeCmd(['joke', 'jokes', 'Joke', 'Jokes']);
siri.takeCmd(['apple', 'Apple']);
okGoogle.takeCmd(['heure', 'hour', 'time']);
okGoogle.takeCmd(['bitcoin', 'crypto', 'btc', 'bit']);
okGoogle.takeCmd(['Android', 'android']);
sam.takeCmd(['weather', 'Weather']);
sam.takeCmd(['chuck', 'Norris', 'Chuck', 'norris', 'Chuck Norris', 'chuck norris']);
chat.takeBots([siri, okGoogle, sam]);
