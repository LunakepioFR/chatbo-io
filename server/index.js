const io = require('socket.io')(8080);

io.on('connection', (socket) => {
  console.log(socket.id);
  socket.on('send-message', (message) => {
    console.log(message.text);
    socket.broadcast.emit('receive-message', message.text);
  });
});
